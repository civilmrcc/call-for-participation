# Call for participation: Save lives with code!

## Help us build a software platform to coordinate Search-And-Rescue operations in the Mediterranean Sea.

Hi from OneFleet and SARchive!

We are a team of open-source software developers who aim to improve communication between various actors involved in civilian rescue operations at sea. We're currently building a live geographic information system ("OneFleet") that helps different NGOs track their ships and better coordinate their missions. It enables them to communicate time-critical geographical information more efficiently, thereby increasing the chances of rescue for boats in distress.

As live distress cases end, we track their outcome and archive reliable information about them in our static research database ("SARchive"). This provides reliable data and statistical evidence for future follow-ups, scientific research and the monitoring of human rights violations at sea.

We are endorsed by multiple Search-and-Rescue NGOs and OneFleet already won some basic financial backing as part of Round 7 of the Open Knowledge Foundation's Prototype Fund in 2020.

<span style="color:#17affa">#SafePassage</span>

## We are looking for you!

Both OneFleet and SARchive are in active development and parts of them are already being tested. But there is still a lot to do! Therefore, we are looking for new members in our team. We are looking for people who have some free time (or more) to support our open-source projects and thereby help improve the work of NGOs in the field of Search-And-Rescue.

![Onefleet](https://gitlab.com/civilmrcc/onefleet/-/raw/develop/pictures/screenshot.png "Onefleet web application")

## How we work and how you can help

We meet up online for one hour every week to discuss the current state, issues and developments. We roughly try to stick to a sprint cycle of four weeks. In between, we sometimes meet spontaneously, code together, and sometimes have a beer and a little chat along that.

If you were to join our team, it would be great if you could join the weekly meetings. Your main tasks would be working on open issues, fixing bugs and implementing new features.

## Skills we crave

Any of the following skills will be very useful, but if you have other things to offer, don't hesitate to contact us!

- **TypeScript / VueJS**
- **Security engineers**
- **CouchDB authentification and user management**
- **Testing and code quality**

## Our Stack

### OneFleet

- VueJS with Element UI framework
- node.js + TypeScript
- Leaflet framework to display the maps
- Microservices for data intake
- Backend: CouchDB

### SARchive

- VueJS with Bootstrap UI framework
- TypeScript as the programming language
- Backend: Python 3 + GraphQL API + PostGIS

## What to expect :)

We are a small team with different backgrounds. Some of us have lots of experience, others have just started out. Some of us can invest a lot of time, some can't. But we all take the project very seriously and are excited to see it in use! In our meetings, there is always room to bring up your own ideas, and to implement them. As part of our team, you would support SAR operations in the Mediterranean and gain insights into the work of NGOs in the field.

## How to reach us

If you are interested, send an e-mail to [mareike@sea-watch.org](mailto:mareike@sea-watch.org). We have prepared **screencasts** to introduce you to the source code and to provide further information about OneFleet and SARchive. You can find the screencasts here:

- Sarchive Frontend: https://youtu.be/q-XRbZeMzw8
- Sarchive Backend: https://youtu.be/szZ9nk5y9uo
- Onefleet: https://youtu.be/u5PkzMBMaXg

If you are still interested after watching these and would like to get to know our team, you are welcome to join a Q&A session in the coming weeks!

Further, you can find the projects on gitlab: https://gitlab.com/civilmrcc/
You might as well have a look at the issues tagged "Onboarding" in our repos.
